<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index ()
    {
        return view('karma.home.home');
    }

    public function category()
    {
      return view('karma.home.category');
    }

    public function product()
    {
        return view('karma.home.product');
    }
    public function  checkout ()
    {
        return view('karma.home.checkout');
    }
    public function shopcart ()
    {
        return view('karma.home.shopcart');
    }
    public function confirmation()
    {
        return view('karma.home.confirmation');
    }



}
