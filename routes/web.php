<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[
 'uses'=>'MainController@index',
 'as'=>'home'
]);
//
Route::get('/category',[
    'uses'=>'MainController@category',
    'as'=>'category'
]);

Route::get('/product',
    [
       'uses'=>'MainController@product',
        'as'=>'product'
    ]);
Route::get('/checkout',[
    'uses'=>'MainController@checkout',
    'as'=>'checkout'
]);

Route::get('/shoppingcart',[
    'uses'=>'MainController@shopcart',
    'as'=>'shopcart'
]);

Route::get('/confirmation',[
    'uses'=>'MainController@confirmation',
    'as'=>'confirmation'
]);

Route::get('/blog',[
    'uses'=>'blogController@blogmain',
    'as'=>'blog'
]);

Route::get('/single-blog',[
    'uses'=>'blogController@singleblog',
    'as'=>'single-blog'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
